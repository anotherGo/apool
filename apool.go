package apool

import (
	"context"
	"runtime"

	lfqueue "github.com/xiaonanln/go-lockfree-queue"
)

type Func struct {
	Fn     func(...interface{})
	queue  *lfqueue.Queue
	Closed bool
}

func NewFunc(Fn func(...interface{}), cap uint16) *Func {
	return &Func{Fn, lfqueue.NewQueue(int(cap)), false}
}
func (f *Func) StartNewWorker(ctx context.Context) error {
	for {
		if f.Closed {
			return ErrClosed
		}
		if ctx.Err() != nil {
			return ctx.Err()
		}
		elem, ok := f.queue.Get()
		if ok == false {
			continue
		}
		f.Fn(elem.([]interface{})...)
		runtime.Gosched()
	}
}
func (f *Func) QueueSize() uint {
	return uint(f.queue.Size())
}
func (f *Func) Submit(args ...interface{}) {
	for {
		f.Closed = false
		ok := f.queue.Put(args)
		if ok == false {
			runtime.Gosched()
		}
	}
}
