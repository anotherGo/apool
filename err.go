package apool

import "errors"

var ErrClosed = errors.New("closed")
